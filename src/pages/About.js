import React, { useContext } from "react";
import { UserContext } from "../providers/UserContext";
import { Switch, Route, Link } from "react-router-dom";

export const About = () => {
  const { user, setUser } = useContext(UserContext);
  return (
    <div>
      <h2>About</h2>
      <pre>{JSON.stringify(user, null, 2)}</pre>

      <nav>
        <ul>
          <li>
            <Link to="/about">about 1</Link>
          </li>
          <li>
            <Link to="/about/about-2">about 2</Link>
          </li>
        </ul>
      </nav>

      <Switch>
        <Route path="/about/about-2">
          <Home />
        </Route>
        <Route path="/">
          <Users />
        </Route>
      </Switch>
    </div>
  );
};

function Users() {
  return <h2>about-1</h2>;
}

function Home() {
  return (
    <div>
      <h2>about-2</h2>

      <nav>
        <ul>
          <li>
            <Link to="/about/about-2">Sub Nav 2</Link>
          </li>
          <li>
            <Link to="/about/about-2/about-3">Sub Nav 3</Link>
          </li>
        </ul>
      </nav>

      <Switch>
        <Route path="/about/about-2/about-3">
          <Others3 />
        </Route>
        <Route path="/about/about-2">
          <Others2 />
        </Route>
      </Switch>
    </div>
  );
}

function Others2() {
  return <h2>Others2</h2>;
}

function Others3() {
  return <h2>Others3</h2>;
}

export default About;
